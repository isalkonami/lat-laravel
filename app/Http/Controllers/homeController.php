<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class homeController extends Controller
{
    public function home()
    {
        return view('home');
    }
    public function register()
    {
        return view('register');
    }
    public function kirim(Request $request)
    {
        $nama = $request->first;

        return view('welcome', compact('nama'));
    }
}
